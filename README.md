# README #

This is a maven project setup as a WAR packaging. To start you can use Tomcat or any other servlet container.

Open your web browser to

http://localhost:8080/   - for the root of the project

http://localhost:8080/site/file/upload   - upload files

http://localhost:8080/site/file/get   - get a list of all entries