package ua.kiev.dyban.exception;



public class RecordNotExistException extends Exception {

    public RecordNotExistException() {
        super();
    }

    public RecordNotExistException(String message) {
        super(message);
    }

    public RecordNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

}
