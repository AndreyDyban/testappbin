package ua.kiev.dyban.model;


import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="Records")
public class Record {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonIgnore
    private int id;

    @Column(name="msg", nullable = false, unique = true)
    private String value;

    @Column(name="quantity", nullable = false)
    private int count;

    public Record() {
    }

    public Record(String value, int count) {
        this.value = value;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", count=" + count +
                '}';
    }
}