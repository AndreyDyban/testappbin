package ua.kiev.dyban.service;


import ua.kiev.dyban.exception.RecordNotExistException;
import ua.kiev.dyban.model.Record;

import java.util.List;

public interface RecordService {

    Record findByValue(String msg) throws RecordNotExistException;

    void update(Record record);

    void save(Record record);

    List<Record> findAll();

}
