package ua.kiev.dyban.service.jpa;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.dyban.exception.RecordNotExistException;
import ua.kiev.dyban.service.RecordService;
import ua.kiev.dyban.model.Record;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Service("recordService")
@Repository
@Transactional
public class RecordServiceJPA implements RecordService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Record findByValue(String msg) throws RecordNotExistException {
        Query query = entityManager.createQuery("SELECT c FROM Record c WHERE c.value = :value");
        query.setParameter("value", msg);
        try {
            return (Record) query.getSingleResult();
        } catch (NoResultException e) {
            throw new RecordNotExistException("Record does not exist : value = " + msg, e);
        }
    }

    @Override
    public void update(Record record) {
        entityManager.merge(record);
    }

    @Override
    public void save(Record record) {
        entityManager.persist(record);
    }

    @Override
    public List<Record> findAll() {
        return entityManager.createQuery("SELECT c FROM Record c").getResultList();
    }
}
