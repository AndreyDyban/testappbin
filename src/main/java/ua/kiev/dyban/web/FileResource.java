package ua.kiev.dyban.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ua.kiev.dyban.exception.RecordNotExistException;
import ua.kiev.dyban.model.Record;
import ua.kiev.dyban.service.RecordService;
import ua.kiev.dyban.service.jpa.RecordServiceJPA;

@Component
@Path("/file")
public class FileResource {

    public static final int MAX_QNT_FILES = 3;

    @Autowired
    public RecordService recordService;


    /**
     * Returns List<Record>, if there are no entries, returns an empty list.
     *
     * @return list of records
     */
    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Record> getProductInJSON() {
        return recordService.findAll();
    }

    /**
     * Saves files to the database.
     * @param input form data
     * @return response
     */
    @POST
    @Path("/upload")
    @Consumes("multipart/form-data")
    public Response uploadFile(MultipartFormDataInput input) {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> form = uploadForm.get("file");
        if (form.size() > MAX_QNT_FILES) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        for (InputPart file : form) {
            try {
                saveFileStrings(file);
            } catch (IOException e) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
        return Response.status(200).entity("Files uploaded").build();

    }

    private void saveFileStrings(InputPart file) throws IOException {
        List<String> lines = IOUtils.readLines(file.getBody(InputStream.class, null));
        for (String string : lines) {
            addLine(string);
        }
    }

    private void addLine(String message) {
        try {
            Record line = recordService.findByValue(message);
            line.setCount(line.getCount() + 1);
            recordService.update(line);
        } catch (RecordNotExistException e) {
            recordService.save(new Record(message, 1));
        }
    }

}